import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import '@babel/polyfill'
import axios from 'axios'
import Vueaxios from 'vue-axios'

Vue.config.productionTip = false
Vue.use(Vueaxios, axios)

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')

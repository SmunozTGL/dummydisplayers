import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

const tgc = {
    primary: "#23c577", // primary main
    primarylight: "#23c577", // primary light
    primarydark: "#23c577", // primary dark
    secondary: "#23c577", // secondary main
    secondarylight: "#23c577", // secondary light
    secondarydark: "#23c577", // secondary dark
    anchor: "#23c577" // link
}

export const theme = {
    ...tgc
}

export default new Vuetify({
    icons: {
        iconfont: 'mdiSvg',
    },
    theme: {
        themes: {
            light: {
                ...theme
            }
        },
    },
});
